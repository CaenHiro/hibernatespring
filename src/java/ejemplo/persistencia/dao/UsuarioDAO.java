package ejemplo.persistencia.dao;

import ejemplo.dominio.Usuario;
import com.ejemplo.persistencia.dao.GenericDAO;

public interface UsuarioDAO extends GenericDAO<Usuario,Integer>  {

}
