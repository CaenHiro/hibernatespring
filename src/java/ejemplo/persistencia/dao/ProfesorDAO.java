package ejemplo.persistencia.dao;

import ejemplo.dominio.Profesor;
import com.ejemplo.persistencia.dao.GenericDAO;

public interface ProfesorDAO extends GenericDAO<Profesor,Integer> {

}
