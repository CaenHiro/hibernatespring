package ejemplo.persistencia.dao.impl;

import ejemplo.persistencia.dao.ProfesorDAO;
import ejemplo.dominio.Profesor;
import com.ejemplo.persistencia.dao.impl.GenericDAOImplHibernate;

public class ProfesorDAOImplHibernate extends GenericDAOImplHibernate<Profesor,Integer> implements  ProfesorDAO {

}
